# -*- coding: utf-8 -*-
"""

@author: Konstantin Petrov

Все счетчики в метрике
"""

import requests
import json
import pandas as pd
import datetime
import sys
import re
import os.path
import chardet
import logging
from time import sleep
import numpy as np
import smtplib
from email.mime.text import MIMEText  
from email.header    import Header
from math import asin

site_csv=os.getcwd()+"/sites.csv"
out_csv=os.getcwd()+"/metrika.csv"
log_txt=os.getcwd()+"/log.txt"
start_date=str(datetime.datetime.today().date() - datetime.timedelta(days=1))
slice_days=str(datetime.datetime.today().date() - datetime.timedelta(days=14))
end_date='yesterday'
week_day_send=1





# add filemode="w" to overwrite
logger = logging.getLogger()
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.INFO, filename = log_txt)



def predict_encoding(file_path, n_lines=20):
    with open(file_path, 'rb') as f:
        # Join binary lines for specified number of lines
        rawdata = b''.join([f.readline() for _ in range(n_lines)])
    return chardet.detect(rawdata)['encoding']

try:
    sites=pd.read_csv(site_csv, encoding=predict_encoding(site_csv),sep=";")
except FileNotFoundError:
    print("Нет файла sites.csv >> [Number:Site] c номерами счетчиков")
    logging.error("Отсутствует файл sites.txt")
    sleep(2)
    sys.exit()


sites.drop_duplicates(subset=['Number'], keep='first', inplace=True)
sites.loc[sites[sites.isna()['Site']].index]['Site']=sites[sites.isna()['Site']]['Number']
listOfSites=list([str(i) for i in sites.Number])
token = ''

for site in listOfSites:
    r=requests.get('https://api-metrika.yandex.ru/analytics/v3/data/ga?end-date='+end_date+'&ids=ga%3A'+site+'&dimensions=ga%3Adate,ga%3AdeviceCategory,ga%3AsourceMedium,ga:landingPagePath&metrics=ga%3Apageviews,ga%3Asessions,ga%3Ausers,ga%3Abounces,ga%3ApageviewsPerSession,ga%3AavgTimeOnPage&start-date='+start_date+'&oauth_token=' + token)

    parsed = json.loads(r.text)

    try:
        df=pd.DataFrame.from_dict(parsed['rows'])
    except KeyError:
        logging.info("Отсутствует счетчик:"+str(site))
        sleep(5)
		#sys.exit()
        next
    try:
        df.columns=[h['name'] for h in parsed['columnHeaders']]
        df['site']=site
    except ValueError:
        logging.info("Отсутствуют значения у счетчика:"+str(site))
        next
    except KeyError:
        next
    try:
        if("out" in locals()):
            out=pd.concat([out,df], axis=0)
        else:
            out=df.copy()
    except NameError:
        next
    sleep(2)
		
try:
    out['ga:date']=pd.to_datetime(out['ga:date'])
except NameError:
    logging.info('Нет данных')
    print('Нет данных')
    sleep(2)
    sys.exit()
num_columns=['ga:pageviewsPerSession','ga:avgTimeOnPage']


try:
    for num in num_columns:
        out[num]=round(pd.to_numeric(out[num]),2)
except ValueError as e:
    if (str(e).find('NaN')):
            nan_indexes=[int(i) for i in re.findall('(\d+)', str(e.args))]
            out=out.drop(out.iloc[nan_indexes].index)
            logging.info("Из-за ошибки в значениях были удалены данные")

out=out.reset_index(drop=True)
for num in sites['Number']:
    indx=list(out[out['site']==str(num)].index)
    s=sites[sites['Number']==num]['Site'].iat[0]
    out.site.replace([str(num)], [s], inplace=True) 



    
if os.path.exists(out_csv):
    out.to_csv(out_csv,mode='a',header=False)
    logging.info("Файл отчета не был создан, создаю")
else:
    out.to_csv(out_csv)



if(datetime.datetime.now().isoweekday()==week_day_send):
    out=pd.read_csv(out_csv,sep=",")
    out=out.set_index(out.columns[0],drop=True)
    listSites=pd.unique(out['site'])
    
    def sendEMail(text, fr="i@dveri5.spb.ru",to=["login@gmail.com","2login@gmail.com"]):
        try:
            server = smtplib.SMTP("smtp.beget.com",2525)
            server.ehlo()
            server.starttls()
            server.login(fr, "i123")
            msg = MIMEText(text, 'html', 'utf-8')
            msg['Subject'] = Header('Trend of sites', 'utf-8')
            msg['From'] = fr
            msg['To'] = ','.join(to)
            server.sendmail(fr,to , msg.as_string())
            server.quit()
            logging.info("Выслан отчет")
        except Exception as e:
            logging.info("Ошибка с отправкой писем", e)
    
    ch=pd.DataFrame(columns=['site','ga:sessions','trend_degrees','min_trend','max_trend',"delta"]);
    #import matplotlib.pyplot as plt
    for site in listSites:
        try:
            o=out[out['site']==site]
            o=o[o['ga:date']>slice_days]           
            del o['ga:deviceCategory']
            del o['ga:sourceMedium']
            o['ga:sessions']=pd.to_numeric(o['ga:sessions'])
            o['ga:users']=pd.to_numeric(o['ga:users'])
            o['ga:bounces']=pd.to_numeric(o['ga:bounces'])
            o['ga:pageviews']=pd.to_numeric(o['ga:pageviews'])
            o=o.groupby('ga:date').sum()
            o.reset_index(inplace=True)
            o=o.sort_values(['ga:date'])
            o['site']=site
            a=np.array([int(i) for i in o['ga:sessions']])
            b=np.array(range(len(a)))
            
            z = np.polyfit(b, a,1)
            p = np.poly1d(z)
            #plt.scatter(b, a)
            #plt.plot(b,p(b),"r--")
            #plt.xlabel('b')
            #plt.ylabel('a')
            #plt.show()
            catets=b[-1]
            h=p(b)[-1]-p(b)[0]
            gip=np.sqrt(b[-1]**2+(p(b)[0]-p(b)[-1])**2)
            deg=round(np.degrees(asin((p(b)[-1]-p(b)[0])/gip)),2)
            sessions=np.sum([int(i) for i in o['ga:sessions']])
            delta=100*p(b)[-1]/p(b)[0]
            if (sessions>0):
                ch.loc[len(ch)]=[site,sessions,deg,p(b)[0],p(b)[-1],delta]
                print([site,sessions,deg,p(b)[0],p(b)[-1],delta])
        except Exception as e:
            print(e)
            pass
    ch=ch.dropna()
    if(len(ch)>0):
        try:
            ch.sort_values(by=['delta'],inplace=True,ascending=False)
            ch['trend_degrees']=round(ch['trend_degrees'],2)
        except AttributeError:
            pass
        ch_msg=str(ch.to_html(show_dimensions=True))
        sendEMail(ch_msg)
    else:
        logging.INFO('Сводная таблица по проектам пуста, возможно мало данных')